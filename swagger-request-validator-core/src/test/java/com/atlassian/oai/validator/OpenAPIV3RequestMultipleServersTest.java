package com.atlassian.oai.validator;

import com.atlassian.oai.validator.model.Request;
import com.atlassian.oai.validator.model.SimpleRequest;
import org.junit.Test;

import static com.atlassian.oai.validator.util.ValidatorTestUtil.assertPass;

public class OpenAPIV3RequestMultipleServersTest {

    private final OpenApiInteractionValidator classUnderTest =
            OpenApiInteractionValidator.createForSpecificationUrl("/oai/v3/api-with-multiple-servers.yaml").build();

    @Test
    public void validate_withTheFirstServer_shouldSucceed() {
        final Request request = SimpleRequest.Builder
                .get("/path/users")
                .build();

        assertPass(classUnderTest.validateRequest(request));
    }

    @Test
    public void validate_withTheSecondServer_shouldSucceed() {
        final Request request = SimpleRequest.Builder
                .get("/alternative-path/users")
                .build();

        assertPass(classUnderTest.validateRequest(request));
    }
}
